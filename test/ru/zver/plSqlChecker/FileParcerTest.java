package ru.zver.plSqlChecker;

import org.junit.Test;

import static org.junit.Assert.fail;

public class FileParcerTest {

    @Test
    public void parceFile() {

        //Проверка на работу с комментариямм
        FileParcer commentParcer = new FileParcer("test//comments_test.txt");
        commentParcer.parceFile();

        ProcedurePlSqlTree commentTree = new ProcedurePlSqlTree("correctOne", 12);
        commentTree.addBegin(13);
        commentTree.setReturnLine(15);
        commentTree.addEnd(16);

        if (!commentParcer.getProcedurePlSqlTreeList().get(0).equals(commentTree))
            fail();

        FileParcer ifParcer = new FileParcer("test//ifTest.sql");
        ifParcer.parceFile();

        ProcedurePlSqlTree ifTree = new FunctionPlSqlTree("onlyIfnodeReturns", 4, "dummy type");
        ifTree.addBegin(6);
        ifTree.addNode(ProcedurePlSqlTree.NodeType.IF,8);
        ifTree.setReturnLine(9);
        ifTree.addNode(ProcedurePlSqlTree.NodeType.ELSE,10);
        ifTree.closeIfNode(13);
        ifTree.addEnd(18);

        if(!ifParcer.getPackageName().equals("test_if"))
            fail(ifParcer.getPackageName());

        if(ifTree.getBeginWithNoEndCnt() < 0 || ifParcer.getProcedurePlSqlTreeList().get(0).getBeginWithNoEndCnt() < 0 ){
            fail();
        }

        if(!ifParcer.getProcedurePlSqlTreeList().get(0).equals(ifTree)){
            fail();
        }

        ifTree = new ProcedurePlSqlTree("bothNodesReturn", 19);
        ifTree.addBegin(21);
        ifTree.addNode(ProcedurePlSqlTree.NodeType.IF,23);
        ifTree.setReturnLine(24);
        ifTree.addNode(ProcedurePlSqlTree.NodeType.ELSE,25);
        ifTree.setReturnLine(27);
        ifTree.closeIfNode(28);
        ifTree.addEnd(30);

        if(!ifParcer.getProcedurePlSqlTreeList().get(1).equals(ifTree)){
            fail();
        }

        FileParcer unavailParcer = new FileParcer("test//unavailableCodeTest.sql");
        unavailParcer.parceFile();

        ProcedurePlSqlTree unavailCodeTree = new ProcedurePlSqlTree("unavailCodeProcedureTest", 3);
        unavailCodeTree.addBegin(5);
        unavailCodeTree.setReturnLine(7);
        unavailCodeTree.setLastCodeLine(9);
        unavailCodeTree.addEnd(11);

        if(!unavailParcer.getProcedurePlSqlTreeList().get(0).equals(unavailCodeTree)){
            fail();
        }

        unavailCodeTree = new ProcedurePlSqlTree("unavailCodeProcedureTestWithExceptionBlock", 13);
        unavailCodeTree.addBegin(15);
        unavailCodeTree.setReturnLine(17);
        unavailCodeTree.addNode(ProcedurePlSqlTree.NodeType.EXCEPTION, 19);
        unavailCodeTree.setLastCodeLine(20);
        unavailCodeTree.addEnd(22);

        if(unavailCodeTree.getErrorMesageList().size() > 0){
            fail("Ошибок быть не должно");
        }

        if(!unavailParcer.getProcedurePlSqlTreeList().get(1).equals(unavailCodeTree)){
            fail();
        }

        FileParcer anonimousBlockTest = new FileParcer("test//anonimousBlockTest.txt");
        anonimousBlockTest.parceFile();

        if(anonimousBlockTest.getProcedurePlSqlTreeList().get(0).getErrorMesageList().size() != 0)
            fail();
    }
}