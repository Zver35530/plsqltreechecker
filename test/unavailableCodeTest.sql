create package body unavailTest is

procedure unavailCodeProcedureTest() is

begin

return;

log();

end;

procedure unavailCodeProcedureTestWithExceptionBlock() is

begin

return;

exception
    when other then null;

end;