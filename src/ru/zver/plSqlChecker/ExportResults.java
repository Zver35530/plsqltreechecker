package ru.zver.plSqlChecker;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class ExportResults {

    public static void export(List<FileParcer> fileParcerList){
        try(BufferedWriter exportWriter = new BufferedWriter(new FileWriter(new File("res/export.txt")))){
            for(FileParcer parcer: fileParcerList){
                if(parcer.getTreesWithTroubles().size() == 0){
                    continue;
                }
                exportWriter.write(parcer.getFilename());
                for(ProcedurePlSqlTree tree: parcer.getTreesWithTroubles()){
                    exportWriter.write("\n\t" + tree.startLine + " " +  tree.name);
                    for( ProcedurePlSqlTree.Message message: tree.getErrorMesageList()){
                        exportWriter.write("\n\t\t" + message.toString());
                    }
                    exportWriter.flush();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
