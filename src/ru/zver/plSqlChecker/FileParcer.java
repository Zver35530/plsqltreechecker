package ru.zver.plSqlChecker;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Pattern;

import static ru.zver.plSqlChecker.ProcedurePlSqlTree.NodeType.EXCEPTION;

public class FileParcer {

    enum Status{
        SUCCESS{
            @Override
            public String toString() {
                return "Успешно";
            }
        },

        NO_PACKAGE_BODY{
            @Override
            public String toString() {
                return "Нет тела пакета";
            }
        },

        ERROR{
            @Override
            public String toString() {
                return "Ошибка";
            }
        }
    }

    private List<ProcedurePlSqlTree> procedurePlSqlTreeList;
    private ProcedurePlSqlTree curTree;
    private Stack<ProcedurePlSqlTree> procedurePlSqlTreeStack = new Stack<>();
    private final String fileName;
    private String packageName;
    private Status status = Status.NO_PACKAGE_BODY;
    private ProcedurePlSqlTree packageTree; //Для хранения строка на уровне пакета

    public String getStatus() {
        return status.toString();
    }

    public String getPackageName() {
        return packageName;
    }

    public String getFilename() {
        return fileName;
    }

    public FileParcer(String filename) {
        this.fileName = filename;
    }

    public List<ProcedurePlSqlTree> getProcedurePlSqlTreeList() {
        return procedurePlSqlTreeList;
    }

    public void parceFile(){
        procedurePlSqlTreeList = new ArrayList<>();
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String curLine;
            int lineNmb = 0;
            String[] splitedString;
            Pattern bodystart = Pattern.compile("create (or replace )*(editonable |unediatonable )*package body");

            boolean isMultilineComment = false;
            boolean isBodyStarted = false;

            while( (curLine = bufferedReader.readLine()) != null){
                ++lineNmb;

                //Удаление комментариев
                {
                    curLine = curLine.replaceAll("--.*", "");
                    curLine = curLine.replaceAll("/\\*.*\\*/", "");

                    if (curLine.indexOf("/*") != -1) {
                        isMultilineComment = true;
                        curLine = curLine.replaceAll("/\\*.*", "");
                    }

                    if (curLine.indexOf("*/") != -1) {
                        isMultilineComment = false;
                        curLine = curLine.replaceAll(".*\\*/", "");
                    }

                    //Исключаем строковые литералы
                    curLine = curLine.replaceAll("'.*'","");

                    if (isMultilineComment&curLine.length() < 2)
                        continue;
                }

                //Проверка на начало тела пакета
                if(!isBodyStarted) {
                    if (bodystart.matcher(curLine).find()) {
                        isBodyStarted = true;
                        packageName = curLine.substring(curLine.indexOf("body") + 4, curLine.indexOf("is")).trim();
                        packageTree = new ProcedurePlSqlTree(packageName, lineNmb);
                        curTree = packageTree;
                        continue;
                    } else
                        continue;
                }

                curLine = curLine.replaceAll("\\s+;",";");
                curLine = curLine.replaceAll("end\\s+if\\s*;","end if;");
                curLine = curLine.replaceAll("\\(", " (");
                splitedString = curLine.trim().split("\\s+");

                switch (splitedString[0].toLowerCase().trim()){

                    //todo Добавить обработку вложенных процедур
                    case "procedure":

                        curTree = new ProcedurePlSqlTree( splitedString[1].split("\\(")[0], lineNmb);
                        procedurePlSqlTreeList.add( curTree);
                        procedurePlSqlTreeStack.push(curTree);
                        break;

                    //todo Добавить обработку вложенных функций
                    case "function":
                        curTree = new FunctionPlSqlTree( splitedString[1].split("\\(")[0], lineNmb, "dummy type");
                        procedurePlSqlTreeList.add(curTree);
                        procedurePlSqlTreeStack.push(curTree);
                        break;

                    case "declare":
                        break;

                    case "begin":
                        curTree.addBegin(lineNmb);
                        break;

                    case "end;"://конец анонимного блока
                        curTree.addEnd(lineNmb);
                        break;

                    case "end":
                        if (splitedString.length > 1) {
                            if (splitedString[1].toLowerCase().equals("if;")) {
                                curTree.closeIfNode(lineNmb);
                                break;
                            }

                            if (splitedString[1].toLowerCase().equals("loop;")) {
                                break;
                            }

                            if (splitedString[1].replaceFirst(";", "").equals(curTree.name)) {
                                curTree.addEnd(lineNmb);// setEndLine(lineNmb);

                                if(curTree.getEndLine() != 0) {
                                    procedurePlSqlTreeStack.pop();
                                    if(!procedurePlSqlTreeStack.empty())
                                        curTree = procedurePlSqlTreeStack.peek();
                                }

                                break;
                            }

                            if(splitedString[1].replaceFirst(";", "").equals(packageName)){
                                packageTree.addEnd(lineNmb);
                                break;
                            }
                        }

                        break;

                    case "if":
                        curTree.addNode(ProcedurePlSqlTree.NodeType.IF, lineNmb);
                        break;

                    case "else":
                        curTree.addNode(ProcedurePlSqlTree.NodeType.ELSE, lineNmb);
                        break;

                    case "elseif":
                        break;

                    case "return;":
                    case "return": //если есть возвращаемое значение
                        curTree.setReturnLine(lineNmb);
                        break;

                    case "raise":
                    case "raise_application_error":
                        curTree.setRaiseLine(lineNmb);
                        break;

                    case "switch":
                        break;

                    case "case":
                        break;

                    case "exception":
                        curTree.addNode(EXCEPTION, lineNmb);
                        break;

                    default:
                        if(curLine.replaceAll("\\s.","").length() > 0)
                            curTree.setLastCodeLine(lineNmb);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            status = Status.ERROR;
        } catch (IOException e) {
            e.printStackTrace();
            status = Status.ERROR;
        } catch (Throwable throwable){
            throwable.printStackTrace();
            status = Status.ERROR;
        }

        if (status != Status.ERROR & packageName != null)
            status = Status.SUCCESS;

        System.out.println(fileName);
        System.out.println(packageName);
        for(ProcedurePlSqlTree tree: procedurePlSqlTreeList){
            System.out.println(tree.toString());
        }
    }

    public List<ProcedurePlSqlTree> getTreesWithTroubles(){
        List<ProcedurePlSqlTree> troubleList = new ArrayList<>();

        for(ProcedurePlSqlTree tree: procedurePlSqlTreeList){
            if(tree.getErrorMesageList().size() != 0){
                troubleList.add(tree);
            }
        }

        return troubleList;
    }

}
