package ru.zver.plSqlChecker;

public enum MessageTypes {
    COULD_NOT_RETURN_VALUE{
        @Override
        public String toString() {
            return "Не все ветки кода возвращают значение";
        }
    }

    , UNAVAILABLE_CODE{
        @Override
        public String toString() {
            return "Недостижимый код";
        }
    }
    , WRONG_RETURN_TYPE{
        @Override
        public String toString() {
            return "Неправильный тип возращаемого значения";
        }
    };

}
