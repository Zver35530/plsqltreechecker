package ru.zver.plSqlChecker;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

public class FileSelector {

    private static final FileFilter filter = new FileFilter() {
        @Override
        public boolean accept(File pathname) {
            if(pathname.isFile()){
                if(pathname.getName().indexOf("~") >= 0)
                    return false;
            }

            return true;
        }
    };

    public static List<File> getFileListIntoDirectoryOrFile(File from){

        List<File> selectedFiles = new ArrayList<>();

        if(from.isFile()){
            selectedFiles.add(from);
            return selectedFiles;
        }

        for (File curFile : from.listFiles( filter )) {
            if(curFile.isFile())
                selectedFiles.add(curFile);
            else
                selectedFiles.addAll(getFileListIntoDirectoryOrFile(curFile));
        }

        return selectedFiles;
    }
}
