package ru.zver.plSqlChecker.gui;

import ru.zver.plSqlChecker.ExportResults;
import ru.zver.plSqlChecker.FileParcer;
import ru.zver.plSqlChecker.FileSelector;
import ru.zver.plSqlChecker.ProcedurePlSqlTree;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MainWindow {

    private List<FileParcer> fileList = new ArrayList<>();
    private final String[] columnNames = new String[]{"Имя файла", "Статус", "Ошибок", "Предупреждений"};
    private JTable fileListTable = new JTable( new DefaultTableModel(columnNames, 1));

    private final String[] errorsColumNames = new String[]{"Название", "Тип", "№ строки"};
    private final DefaultTableModel noErrorsModel = new DefaultTableModel(errorsColumNames, 1);
    private JTable errorsTable = new JTable( noErrorsModel );

    {
        fileListTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int iRow = fileListTable.rowAtPoint(e.getPoint());

                List<ProcedurePlSqlTree> troubleList = fileList.get(iRow).getTreesWithTroubles();
                if(troubleList.size() == 0) {
                    errorsTable.setModel(noErrorsModel);
                    return;
                }

                int curTree = 0;
                String[][] errorValues = new String[troubleList.size()][errorsColumNames.length];
                for(ProcedurePlSqlTree tree:troubleList){
                    errorValues[curTree][0] = tree.name;
                    errorValues[curTree][1] = tree.getErrorMesageList().toString();
                    errorValues[curTree++][2] = String.valueOf(tree.startLine);
                }

                errorsTable.setModel(new DefaultTableModel(errorValues, errorsColumNames));
                mainFraim.repaint();
            }
        });
    }

    private final JFrame mainFraim = new JFrame("Разбор дерева кода PL/SQL");

   // private Map<String, List<Ero>>

    public MainWindow(){
        mainFraim.setVisible(true);
        mainFraim.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFraim.setSize(200, 600);

        mainFraim.setLayout(new BorderLayout());

        {
            JFileChooser fileChooser = new JFileChooser();

            fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

            fileChooser.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    fileList.clear();
                    addFile(FileSelector.getFileListIntoDirectoryOrFile(fileChooser.getSelectedFile()));
                 //   fileList.add(new FileParcer(fileChooser.getSelectedFile().getPath()));
                    showFileList();
                    mainFraim.repaint();
                }
            });
            mainFraim.add(fileChooser, BorderLayout.NORTH);

            mainFraim.add(new JScrollPane(fileListTable), BorderLayout.CENTER);
            mainFraim.add(new JScrollPane(errorsTable), BorderLayout.EAST);
        }
        {
            JPanel controlButtons = new JPanel();
            mainFraim.add(controlButtons, BorderLayout.WEST);
            controlButtons.setLayout(new BoxLayout(controlButtons, BoxLayout.Y_AXIS));
            JButton startParce = new JButton("Начать разбор файлов");
            startParce.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    showFileList();
                }
            });
            controlButtons.add(startParce);

            JButton exportResults = new JButton("Экспорт результатов");
            exportResults.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ExportResults.export(fileList);
                }
            });
            controlButtons.add( exportResults );

        }

        mainFraim.pack();
    }

    public MainWindow(File file){
        this();

        addFile(FileSelector.getFileListIntoDirectoryOrFile(file));

        showFileList();
    }

    public MainWindow(List<File> fileList){
        this();
        addFile(fileList);
        showFileList();
    }

    private void showFileList(){
        String[][] fileValues = new String[fileList.size()][columnNames.length];

        for(FileParcer curFile: fileList){
            curFile.parceFile();
        }

        fileList.sort(new Comparator<FileParcer>() {
            @Override
            public int compare(FileParcer o1, FileParcer o2) {
                if(o1.getTreesWithTroubles() == null)
                    return 1;

                if(o2.getTreesWithTroubles() == null)
                    return 1;

                return o1.getTreesWithTroubles().size() > o2.getTreesWithTroubles().size() ? -1 : 1;
            }
        });

        int curLine = 0;

        for(FileParcer curFile: fileList){
            fileValues[curLine][0] = curFile.getFilename();

            fileValues[curLine][1] = curFile.getStatus();
            fileValues[curLine][2] = String.valueOf(curFile.getTreesWithTroubles().size());

            ++curLine;
        }

        fileListTable.setModel(new DefaultTableModel(fileValues, columnNames));

        mainFraim.repaint();
    }

    public void addFile(File file){
        fileList.add( new FileParcer(file.getPath()));
    }

    public void addFile(List<File> files){
        for(File file: files)
            fileList.add(new FileParcer(file.getPath()) );
    }

    public void clearFileList(){
        fileList.clear();
    }

}
