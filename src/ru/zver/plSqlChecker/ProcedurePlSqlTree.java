package ru.zver.plSqlChecker;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/** Отображает одну функцию или процедуру
 * */
public class ProcedurePlSqlTree {
    public final String name;
    public final int startLine;

    private List<Message> errorMesageList = new ArrayList<>();

    private Stack<AnonimousNode> anonimousNodeStack = new Stack<>();
    protected final AnonimousNode rootNode;

    private IfNode curIfNode;
    private AnonimousNode curNode;

    public ProcedurePlSqlTree(String name, int startLine) {
        this.name = name;
        this.startLine = startLine;

        curNode = rootNode = new AnonimousNode(startLine, null, false);
        anonimousNodeStack.push(rootNode);
    }

    public void addBegin(int line){
        if(curNode.getBeginLine() == -1) { //была секция declare или начало именованного блока
            curNode.setBeginLine(line);
            return;}
        else{
            curNode = new AnonimousNode(line, curNode, true);
            anonimousNodeStack.push(curNode);
        }
    }

    public void addBegin(int line, boolean isDeclare){
        addBegin(line);
        if(isDeclare){
            curNode.setBeginLine(-1);
        }
    }

    public boolean addEnd(int line) {
        curNode.setEndLine(line);
        if(anonimousNodeStack.isEmpty()){
            errorMesageList = checkErrors();
        }
        else {
            anonimousNodeStack.pop();
            if(!anonimousNodeStack.empty())
                curNode = anonimousNodeStack.peek();
        }

        return true;
    }

    public void setRaiseLine(int raiseLine){
        curNode.setRaiseLine(raiseLine);
    }

    public boolean addNode(NodeType type, int line){

        switch (type){
            case IF:
                curIfNode = new IfNode(line, curIfNode != null ? curIfNode : curNode);
                curNode.childNodesList.add(curIfNode);
                return true;

            case ELSE:
                if(curIfNode == null)
                    return false;

                curIfNode.addFalseNode(new PlSqlNode(line, NodeType.ELSE, curNode));
                return true;

            case EXCEPTION:
                curNode.setExceptionNode(line, curNode);
                return true;
        }

        return true;
    }

    @Deprecated
    /**Закрыть блок если.
     * Если нет текущего блока если, то возвращается ложь
     * */
    public boolean closeIfNode(int line){
        if (curIfNode == null){
            return false;
        }

        curIfNode.setEndLine(line);

        curIfNode = curIfNode.getParent().nodeType == NodeType.IF ? (IfNode) curIfNode.getParent() : null;
        return true;
    }

    public int getEndLine() {
        return curNode.endLine;
    }

    public int getBeginWithNoEndCnt() {
        return anonimousNodeStack.size();
    }

    public boolean setReturnLine(int line){
        (curIfNode != null ? curIfNode : rootNode).setReturnLine(line);

        return true;
    }

    public List<Message> getErrorMesageList(){
        return errorMesageList;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(startLine);
        builder.append("\n").append(name);

        builder.append("\nstart_line = ").append(rootNode.startLine);

        for(PlSqlNode node: rootNode.childNodesList){
            builder.append("\n").append(node.toString());
        }

        builder.append("\nend line = ").append(rootNode.endLine);

        builder.append("\ndoHaveAnyReturnTroubles ").append(errorMesageList.size() == 0 ? "false" : "true");
        return builder.toString();
    }

    public boolean equals(ProcedurePlSqlTree obj) {
        return name.equals(obj.name) &&
               startLine == obj.startLine &&
               getEndLine() == obj.getEndLine() &&
               errorMesageList.size() == obj.errorMesageList.size()
               ;
    }

    protected List<Message> checkErrors(){
        List<Message> messageTypesList = new ArrayList<>();

        int unAvailCodeLine;

        for(PlSqlNode node: rootNode.childNodesList){
            unAvailCodeLine = hasUnAvailCode(node);
            if(unAvailCodeLine != -1){
                messageTypesList.add( new Message( MessageTypes.UNAVAILABLE_CODE, unAvailCodeLine));
            }
        }

        unAvailCodeLine = hasUnAvailCode(rootNode);
        if(unAvailCodeLine != -1)
            messageTypesList.add( new Message( MessageTypes.UNAVAILABLE_CODE, unAvailCodeLine));

        return messageTypesList;
    }
    /** -1 если нет ошибки
     * */
    private int hasUnAvailCode(PlSqlNode node){

        if( node.returnLine > 0 &&
            node.lastCodeline > node.returnLine )
            return node.lastCodeline;

        if( node.raiseLine > 0 && node.lastCodeline > node.raiseLine)
            return node.lastCodeline;

        return -1;
    }

    public void setLastCodeLine(final int lineNmb){
        if (curNode.getExeptionLine() == -1)
            curNode.setLastCodeline(lineNmb);
    }

    /**Отображает элемент ветвления: if или switch
     * */
    class PlSqlNode {
        public final int startLine;
        public final NodeType nodeType;
        public final PlSqlNode parent;
        public final int level;
        protected int endLine = -1;
        protected int returnLine = -1;
        protected int raiseLine = -1;
        /**Последняя строка с кодом. Для проверки недостижимого кода
         * */
        protected int lastCodeline;

        public void setRaiseLine(int raiseLine) {
            this.raiseLine = raiseLine;
        }

        protected void setLastCodeline(int lastCodeline) {
            this.lastCodeline = lastCodeline;
        }

        public PlSqlNode(int startLine, NodeType nodeType, PlSqlNode parent) {
            this.startLine = startLine;
            this.lastCodeline = startLine;
            this.nodeType = nodeType;
            switch (nodeType){
                case ANONIMOUS:
                    this.parent = null;
                    level = 0;
                    break;
                default:
                    if(parent != null) {
                        this.parent = parent;
                        level = parent.level + 1;
                    } else
                        throw new NullPointerException("Узел родителя не может быть пустым");
            }
        }

        protected List<IfNode> childNodesList = new ArrayList<>();

        public void setEndLine(int endLine){
            this.endLine = endLine;
        }

        public int getEndLine() {
            return endLine;
        }

        public PlSqlNode getParent() {
            return parent;
        }

        public int getReturnLine() {
            return returnLine;
        }

        public void setReturnLine(int returnLine) {
            this.returnLine = returnLine;
        }

        @Override
        public String toString() {
            return nodeType.toString() + " " + startLine +
                    "\n  endLine" + endLine + (returnLine != 0 ? ("\n  returnLine " + returnLine) : "" );
        }
    }

    public class AnonimousNode extends PlSqlNode{

        private ExceptionNode exceptionNode = null;
        private int beginLine = -1;

        public void setExceptionNode(int line, PlSqlNode parent){
            exceptionNode = new ExceptionNode(line, parent);
        }

        public int getExeptionLine(){
            return this.exceptionNode == null ? -1 : this.exceptionNode.startLine;
        }

        public void setBeginLine(int beginLine) {
            this.beginLine = beginLine;
        }

        public int getBeginLine() {
            return beginLine;
        }

        public AnonimousNode(int startLine, PlSqlNode parent, boolean isBeginLine) {
            super(startLine, NodeType.ANONIMOUS, parent);
            if(isBeginLine)
                setBeginLine(startLine);
        }
    }

    public class IfNode extends PlSqlNode{

        private PlSqlNode elseNode;

        @Override
        public void setReturnLine(int returnLine) {
            (elseNode != null ? elseNode : this).returnLine = returnLine;
        }

        public IfNode(int startLine, PlSqlNode parent) {
            super(startLine, NodeType.IF, parent);
        }

        public void addFalseNode(PlSqlNode plSqlNode){
            elseNode = plSqlNode;
        }

        /**Возвращает последний номер строки, на которой происходит гарантированный возврат результата.
         * То есть обе ветки, если они есть, возвращают значение.
         * Либо возврат значения происходит после блока если-то
         * */
        @Override
        public int getReturnLine() {

            //Для начала, проверяем самый простой случай.
            if(returnLine != -1  ) {
                if (elseNode == null){
                    return returnLine;
                }else if(elseNode.returnLine != -1)
                    return elseNode.returnLine;
            }

            int result = -1;

            /*
            for( IfNode node: elseNode.childNodesList ){
                result = node.returnLine;
            }

            //Сначала обходим положительную ветку
            for(IfNode node: childNodesList){

            }
            */

            return result; // (returnLine != -1 & elseNode.returnLine != -1) ? elseNode.returnLine : -1;
        }

        @Override
        public String toString() {
            return super.toString() + (elseNode == null ? "" : "\nelse " + elseNode.startLine) + "\nend if " + endLine;
        }
    }

    class ExceptionNode extends PlSqlNode{

        List<PlSqlNode> exeptionList = new ArrayList<>();

        public ExceptionNode(int startLine, PlSqlNode parent) {
            super(startLine, NodeType.EXCEPTION, parent);
        }
    }

    enum NodeType{
        ANONIMOUS, IF, ELSE, ELSEIF, SWITCH, EXCEPTION;
    }

    class Message{
        public final MessageTypes type;
        public final int line;

        public Message(MessageTypes type, int line) {
            this.type = type;
            this.line = line;
        }

        @Override
        public String toString() {
            return type.toString() + ": " + line;
        }
    }
}