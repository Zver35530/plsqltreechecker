package ru.zver.plSqlChecker;

import java.util.List;

public class FunctionPlSqlTree extends ProcedurePlSqlTree {
    /**Тип возращаемого значения
     * */
    public final String returnType;

    public FunctionPlSqlTree(String name, int startLine, String returnType) {
        super(name, startLine);
        this.returnType = returnType;
    }

    @Override
    protected List<Message> checkErrors() {
        List<Message> messageTypesList = super.checkErrors();

        if(!doRetunsAnyway())
            messageTypesList.add( new Message( MessageTypes.COULD_NOT_RETURN_VALUE, startLine));

        return messageTypesList;
    }

    private boolean doRetunsAnyway(){

        if(rootNode.getReturnLine() != -1 )
            return true;

        for(PlSqlNode node: rootNode.childNodesList){
            if(node.getReturnLine() != -1 )
                return true;
        }

        return false;
    }
}
